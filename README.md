# RSA-GMP  
  
Programa que cifra/descifra mensajes con el sistema criptográfico RSA usando la librería GMP (https://gmplib.org/). En otro programa intenté armar todos los algoritmos de cero pero me limitó el uso de números enteros con precisión limitada. Implementado en base a la explicación de la criptografía RSA dada en el fascículo teórico de la materia Álgebra 1 dada en FCEN, UBA.  
  
La criptografía RSA es una consecuencia del Pequeño Teorema de Fermat, y también aprovecha el algoritmo de Euclides, el test de primalidad Miller-Rabin, etc.  
  
## Requisitos:  
- linux (por el uso de getrandom())  
- libgmp  
- gcc u otro compilador compatible  