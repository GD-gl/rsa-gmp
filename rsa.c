#include <gmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <sys/random.h>

void genprime(mpz_t numeroRet, gmp_randstate_t randstate, size_t bits) {

    mpz_t numero;
    mpz_init(numero);

    // genero numero random de 0 a 2^bits - 1
    mpz_urandomb(numero, randstate, bits);

    // si ya es primo, lo retorna
    if (mpz_probab_prime_p(numero, 24) == 2) {
        mpz_set(numeroRet, numero);
        mpz_clear(numero);
        return;
    }

    // retorna el siguiente primo
    mpz_nextprime(numeroRet, numero);
    mpz_clear(numero);

}

int main() {

    mpz_t phi, n, pm1, qm1, p, q, e, d, tmp1, tmp2;
    gmp_randstate_t rstate;
    unsigned long int seed;
    const size_t bits = 1024 * 8;

    mpz_inits(p, q, phi, n, pm1, qm1, e, d, tmp1, tmp2, NULL);

    // configuro el generador de numeros aleatorios
    gmp_randinit_default(rstate);
    getrandom(&seed, sizeof(seed), 0);
    gmp_randseed_ui(rstate, seed);

    // preparo p, q (numeros primos random)
    time_t begin_genprime = time(NULL);
    genprime(p, rstate, bits);
    genprime(q, rstate, bits);
    time_t end_genprime = time(NULL);

    // calculo n y (p-1)(q-1) = phi
    time_t begin_nphi = time(NULL);
    mpz_sub_ui(pm1, p, 1); // pm1 = p - 1
    mpz_sub_ui(qm1, q, 1); // qm1 = q - 1
    mpz_mul(n, p, q); // n = p * q
    mpz_mul(phi, pm1, qm1); // phi = pm1 * qm1
    time_t end_nphi = time(NULL);

    // calculo e
    // genero numeros random entre 1 y phi
    // y me quedo con alguno que sea coprimo con phi
    time_t begin_e = time(NULL);
    do {
        mpz_urandomm(tmp1, rstate, phi); // tmp1 = random entre 0 y phi no inclusive
        mpz_add_ui(e, tmp1, 1); // e = tmp1 + 1
        mpz_gcd(tmp1, e, phi);
    } while (mpz_cmp_ui(tmp1, 1));
    time_t end_e = time(NULL);

    // calculo d
    // e*d == 0 (mod phi)
    // d == e^-1 (mod phi)
    time_t begin_d = time(NULL);
    mpz_invert(d, e, phi);
    time_t end_d = time(NULL);

    size_t bits_n = bits * 3;
    time_t begin_bits_n = time(NULL);
    do {
        bits_n -= 1;
        mpz_set_ui(tmp1, 2);
        mpz_pow_ui(tmp2, tmp1, bits_n);
    } while (mpz_cmp(n, tmp2) < 0);
    time_t end_bits_n = time(NULL);

    printf("bits = %lu\n", bits);
    gmp_printf("p (%i): %Zd\n", mpz_probab_prime_p(p, 24), p);
    gmp_printf("q (%i): %Zd\n", mpz_probab_prime_p(q, 24), q);
    gmp_printf("n (%i) [%lu bits]: %Zd\n", mpz_probab_prime_p(n, 24), bits_n, n);
    gmp_printf("(p-1) (%i): %Zd\n", mpz_probab_prime_p(pm1, 24), pm1);
    gmp_printf("(q-1) (%i): %Zd\n", mpz_probab_prime_p(qm1, 24), qm1);
    gmp_printf("phi (%i): %Zd\n", mpz_probab_prime_p(phi, 24), phi);
    gmp_printf("e (%i): %Zd\n", mpz_probab_prime_p(e, 24), e);
    mpz_gcd(tmp1, e, phi);
    gmp_printf("gcd(e, phi) = %Zd (nice)\n", tmp1);
    gmp_printf("d (%i): %Zd\n", mpz_probab_prime_p(d, 24), d);

    mpz_mul(tmp1, e, d);
    mpz_mod(tmp2, tmp1, phi);
    gmp_printf("e*d mod phi = %Zd (nice)\n", tmp2);

    const char mensaje[] = "Hola\0\0\0\0";
    const size_t bits_mensaje = (sizeof(mensaje) - 1) * 8;
    if (bits_mensaje >= bits_n) {
        printf("bits_mensaje > bits_n\n");
        return 1;
    }

    // cargo el mensaje a cifrar en tmp1
    mpz_import(tmp1, bits_mensaje / 8, 1, sizeof(char), -1, 0, mensaje);
    gmp_printf("\nmensaje: %Zd (%s)\n", tmp1, mensaje);

    // cifrado = mensaje ^ e mod n
    time_t begin_enc = time(NULL);
    mpz_powm(tmp2, tmp1, e, n);
    time_t end_enc = time(NULL);
    gmp_printf("cifrado: %Zd\n", tmp2);

    // descifrado = mensaje ^ d mod n
    time_t begin_dec = time(NULL);
    mpz_powm(tmp1, tmp2, d, n);
    time_t end_dec = time(NULL);

    char * mensaje_descifrado = calloc(1, (bits_mensaje / 8) + 1);
    mpz_export(mensaje_descifrado, NULL, 1, sizeof(char), -1, 0, tmp1);
    gmp_printf("descifrado: %Zd (%s)\n", tmp1, mensaje_descifrado);

    time_t time_genprime = end_genprime - begin_genprime;
    printf("\ntime_genprime = %lus\n", time_genprime);
    time_t time_nphi = end_nphi - begin_nphi;
    printf("time_nphi = %lus\n", time_nphi);
    time_t time_e = end_e - begin_e;
    printf("time_e = %lus\n", time_e);
    time_t time_d = end_d - begin_d;
    printf("time_d = %lus\n", time_d);
    time_t time_bits_n = end_bits_n - begin_bits_n;
    printf("time_bits_n = %lus\n", time_bits_n);
    time_t time_enc = end_enc - begin_enc;
    printf("time_enc = %lus\n", time_enc);
    time_t time_dec = end_dec - begin_dec;
    printf("time_dec = %lus\n", time_dec);
    
    free(mensaje_descifrado);
    mpz_clears(p, q, phi, n, pm1, qm1, e, d, tmp1, tmp2, NULL);

    return 0;

}
